Feature: validate the cart page

As a new user
I  Validate product information (name/price) between product detail page and cart page
So I can compare the information between these page

Scenario: validate the cart page
Given I am on the lazada page
Given I have to resize to (1300x900)
When I find the product by using the "EY192FAAC5KSVNAMZ-111399" number
Then I should see "EOV8568" as the name for this item
And I should see "499.000 VND" as subtotal for this item 
Then I click on the Buy button
And I should see the cart page appear
And I should see "EOV8568" as the name for this item
And I should see "499.000 VND" as subtotal for this item
And I can compare the information between product detail page and cart page
