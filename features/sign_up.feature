Feature: register account

As a new user
I want to register new account without fill the 2nd password
So I can see the error message after I press send button

Scenario: register account
Given I am on the create account page
Given I have to resize to (1300x900)
When I enter "Bui Dang Binh" in the name field
And I enter "dangbinh_bui@yahoo.com" in the email field
And I enter "12345@?a" in the password field
And I click the Gui button
Then I should see "Passwords did not match"
