require 'rubygems'
require 'watir-webdriver'
@browser= Watir::Browser.new :firefox

SKU = 'EY192FAAC5KSVNAMZ-111399'
HOME_PAGE = 'http://www.lazada.vn/'
CART_PAGE = 'http://www.lazada.vn/cart/'

def search(sku)
  @browser.text_field(:id => 'searchInput').when_present.set(sku)
  @browser.div(:class => 'header__search__submit').when_present.click
end

def open_product_detail
  @browser.element(:class => 'product-card__img').when_present.hover
  @browser.element(:class => 'product-card__img').when_present.click
end

def get_product_info
  @browser.span(:class => 'product-card_name big').text
  @browser.div(:class => 'product-card__price').text
end

def get_product_info_cart_page
  sleep 5
  @browser.element(:class => 'productdescription').text
end

def add_product_to_cart
  @browser.element(:class => 'button-buy__text').when_present.click
end

def open_cart_page
  @browser.element(class: 'cart-popup-2015').when_present{
    @browser.goto(CART_PAGE)
  }
end

Given(/^I am on the lazada page$/) do
  @browser.goto (HOME_PAGE)
end

When(/^I find the product by using the "([^"]*)" number$/) do |number|
  search(SKU)
end

Then(/^I should see "([^"]*)" as the name for this item$/) do |name|
  expect@browser.text.include? 'EOV8568'
end

Then(/^I should see "([^"]*)" as subtotal for this item$/) do |subtotal|
  expect@browser.text.include? '499.000 VND'
end

Then(/^I click on the Buy button$/) do
  @browser.element(:class => 'state_loaded').when_present.hover
  add_product_to_cart
end

Then(/^I should see the cart page appear$/) do
  open_cart_page
end

Then(/^I can compare the information between product detail page and cart page$/) do
  get_product_info == get_product_info_cart_page
end