Given(/^I am on the create account page$/) do
  @browser.goto 'https://www.lazada.vn/customer/account/create/'
end

Given(/^I have to resize to \((\d+)x(\d+)\)$/) do |height, width|
  @browser.window.resize_to(1300, 900)
end

When(/^I enter "([^"]*)" in the name field$/) do |hoten|
  @browser.text_field(:id => 'RegistrationForm_first_name').set(hoten)
end

When(/^I enter "([^"]*)" in the email field$/) do |email|
    @browser.text_field(:id => 'RegistrationForm_email').set(email)
end

When(/^I enter "([^"]*)" in the password field$/) do |matkhau|
  @browser.text_field(:id => 'RegistrationForm_password').set(matkhau)
end

When(/^I click the Gui button$/) do
  @browser.button(:id => 'send').when_present.click
end

Then(/^I should see "([^"]*)"$/) do |expected|
  expect@browser.text.include? 'Passwords did not match'
end